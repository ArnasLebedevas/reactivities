﻿using Reactivities.Domain;
using Reactivities.Domain.UserFollowings;
using Reactivities.Persistence;
using System.Threading.Tasks;

namespace Reactivities.Data.Core.Followers
{
    public class UserFollowingRepository : Repository<UserFollowing>, IUserFollowingRepository
    {
        public UserFollowingRepository(DataContext context) : base(context)
        { }

        public async Task<UserFollowing> FindAsync(string observerID, string targetID)
        {
            return await _context.UserFollowings.FindAsync(observerID, targetID);
        }
    }
}
