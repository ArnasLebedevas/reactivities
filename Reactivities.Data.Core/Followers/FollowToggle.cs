﻿using MediatR;
using Reactivities.Application.Core;
using Reactivities.Domain;
using Reactivities.Domain.User;
using Reactivities.Domain.UserFollowings;
using System.Threading;
using System.Threading.Tasks;

namespace Reactivities.Application.Followers
{
    public class FollowToggle
    {
        public class Command : IRequest<Result<Unit>>
        {
            public string TargetUsername { get; set; }
        }

        public class Handler : IRequestHandler<Command, Result<Unit>>
        {
            private readonly IAppUserRepository _appUserRepository;
            private readonly IUserFollowingRepository _userFollowingRepository;

            public Handler(IAppUserRepository appUserRepository, IUserFollowingRepository userFollowingRepository)
            {
                _appUserRepository = appUserRepository;
                _userFollowingRepository = userFollowingRepository;
            }

            public async Task<Result<Unit>> Handle(Command request, CancellationToken cancellationToken)
            {
                var observer = await _appUserRepository.GetUser();

                var target = await _appUserRepository.GetUserByUsername(request.TargetUsername);

                if (target == null) return null;

                var following = await _userFollowingRepository.FindAsync(observer.Id, target.Id);

                if(following == null)
                {
                    following = new UserFollowing
                    {
                        Observer = observer,
                        Target = target
                    };

                    _userFollowingRepository.Add(following);
                }
                else
                {
                    _userFollowingRepository.Remove(following);
                }

                var success = await _userFollowingRepository.SaveAllAsync();

                if (success) return Result<Unit>.Success(Unit.Value);

                return Result<Unit>.Failure("Failed to update following");
            }
        }
    }
}
