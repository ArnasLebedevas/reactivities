﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Reactivities.Application.Core;
using Reactivities.Application.Interfaces;
using Reactivities.Models.Profile;
using Reactivities.Persistence;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Reactivities.Application.Followers
{
    public class FollowersList
    {
        public class Query : IRequest<Result<List<ProfileModel>>>
        {
            public string Predicate { get; set; }
            public string Username { get; set; }
        }

        public class Handler : IRequestHandler<Query, Result<List<ProfileModel>>>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            private readonly IUserResolver _userResolver;

            public Handler(DataContext context, IMapper mapper, IUserResolver userResolver)
            {
                _context = context;
                _mapper = mapper;
                _userResolver = userResolver;
            }

            public async Task<Result<List<ProfileModel>>> Handle(Query request, CancellationToken cancellationToken)
            {
                var profiles = new List<ProfileModel>();

                switch (request.Predicate)
                {
                    case "followers":
                        profiles = await _context.UserFollowings.Where(x => x.Target.UserName == request.Username)
                            .Select(u => u.Observer)
                            .ProjectTo<ProfileModel>(_mapper.ConfigurationProvider, new { currentUsername = _userResolver.GetUsername() })
                            .ToListAsync();
                        break;
                    case "following":
                        profiles = await _context.UserFollowings.Where(x => x.Observer.UserName == request.Username)
                            .Select(u => u.Target)
                            .ProjectTo<ProfileModel>(_mapper.ConfigurationProvider, new { currentUsername = _userResolver.GetUsername() })
                            .ToListAsync();
                        break;
                }

                return Result<List<ProfileModel>>.Success(profiles);
            }
        }

    }
}
