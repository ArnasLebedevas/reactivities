﻿namespace Reactivities.Application.Interfaces
{
    public interface IUserResolver
    {
        string GetUsername();
    }
}
