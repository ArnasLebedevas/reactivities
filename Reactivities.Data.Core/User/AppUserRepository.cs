﻿using Microsoft.EntityFrameworkCore;
using Reactivities.Application.Interfaces;
using Reactivities.Domain;
using Reactivities.Domain.User;
using Reactivities.Persistence;
using System.Threading.Tasks;

namespace Reactivities.Data.Core.User
{
    public class AppUserRepository : Repository<AppUser>, IAppUserRepository
    {
        private readonly IUserResolver _userResolver;

        public AppUserRepository(DataContext context, IUserResolver userResolver) : base(context)
        {
            _userResolver = userResolver;
        }

        public async Task<AppUser> GetUser()
        {
            return await _context.Users.FirstOrDefaultAsync(x => x.UserName == _userResolver.GetUsername());
        }

        public async Task<AppUser> GetUserByUsername(string username)
        {
            return await _context.Users.FirstOrDefaultAsync(x => x.UserName == username);
        }

        public async Task<AppUser> GetUserWithPhoto()
        {
            return await _context.Users.Include(p => p.Photos).FirstOrDefaultAsync(x => x.UserName == _userResolver.GetUsername());
        }
    }
}
