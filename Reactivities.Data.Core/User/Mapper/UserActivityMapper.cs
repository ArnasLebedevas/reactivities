﻿using AutoMapper;
using Reactivities.Domain;
using Reactivities.Models.Profile;
using System.Linq;

namespace Reactivities.Application.Core.Mapping
{
    public class UserActivityMapper : Profile
    {
        public UserActivityMapper()
        {
            CreateMap<ActivityAttendee, UserActivityModel>()
                .ForMember(d => d.ID, o => o.MapFrom(s => s.Activity.ID))
                .ForMember(d => d.Date, o => o.MapFrom(s => s.Activity.Date))
                .ForMember(d => d.Title, o => o.MapFrom(s => s.Activity.Title))
                .ForMember(d => d.Category, o => o.MapFrom(s => s.Activity.Category))
                .ForMember(d => d.HostUsername, o => o.MapFrom(s => s.Activity.Attendees.FirstOrDefault(x => x.IsHost).AppUser.UserName));
        }
    }
}
