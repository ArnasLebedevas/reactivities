﻿using AutoMapper;
using Reactivities.Domain;
using Reactivities.Models.Profile;
using System.Linq;

namespace Reactivities.Application.Core.Mapping
{
    public class AppUserMapper : Profile
    {
        public AppUserMapper()
        {
            string currentUsername = null;

            CreateMap<AppUser, ProfileModel>()
              .ForMember(d => d.Image, o => o.MapFrom(s => s.Photos.FirstOrDefault(x => x.IsMain).Url))
              .ForMember(d => d.FollowersCount, o=> o.MapFrom(s => s.Followers.Count))
              .ForMember(d => d.FollowingCount, o => o.MapFrom(s => s.Followings.Count))
              .ForMember(d => d.Following, o=> o.MapFrom(s => s.Followers.Any(x => x.Observer.UserName == currentUsername)));
        }
    }
}
