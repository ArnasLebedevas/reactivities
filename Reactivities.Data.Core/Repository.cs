﻿using Reactivities.Domain;
using Reactivities.Persistence;
using System.Threading.Tasks;

namespace Reactivities.Data.Core
{
    public class Repository<TEntity> : IRepository<TEntity>
         where TEntity : class
    {
        protected DataContext _context;

        public Repository(DataContext context)
        {
            _context = context;
        }

        public void Add(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
        }

        public void Remove(TEntity entity)
        {
            _context.Set<TEntity>().Remove(entity);
        }

        public async Task<bool> SaveAllAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}
