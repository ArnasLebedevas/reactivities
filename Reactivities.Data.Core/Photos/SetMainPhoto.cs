﻿using MediatR;
using Reactivities.Application.Core;
using Reactivities.Domain.User;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Reactivities.Application.Photos
{
    public class SetMainPhoto
    {
        public class Command : IRequest<Result<Unit>>
        {
            public string ID { get; set; }
        }

        public class Handler : IRequestHandler<Command, Result<Unit>>
        {
            private readonly IAppUserRepository _appUserRepository;

            public Handler(IAppUserRepository appUserRepository)
            {
                _appUserRepository = appUserRepository;
            }

            public async Task<Result<Unit>> Handle(Command request, CancellationToken cancellationToken)
            {
                var user = await _appUserRepository.GetUserWithPhoto();

                if (user == null) return null;

                var photo = user.Photos.FirstOrDefault(x => x.ID == request.ID);

                if (photo == null) return null;

                var currentMain = user.Photos.FirstOrDefault(x => x.IsMain);

                if (currentMain != null) currentMain.IsMain = false;

                photo.IsMain = true;

                var success = await _appUserRepository.SaveAllAsync();

                if (success) return Result<Unit>.Success(Unit.Value);

                return Result<Unit>.Failure("Problem setting main photo");
            }
        }
    }
}
