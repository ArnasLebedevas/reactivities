﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Reactivities.Application.Core;
using Reactivities.Application.Interfaces;
using Reactivities.Domain;
using Reactivities.Domain.User;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Reactivities.Application.Photos
{
    public class AddPhoto
    {
        public class Command : IRequest<Result<Photo>>
        {
            public IFormFile File { get; set; }
        }

        public class Handler : IRequestHandler<Command, Result<Photo>>
        {
            private readonly IPhotoResolver _photoResolver;
            private readonly IAppUserRepository _appUserRepository;

            public Handler(IPhotoResolver photoResolver, IAppUserRepository appUserRepository)
            {
                _photoResolver = photoResolver;
                _appUserRepository = appUserRepository;
            }

            public async Task<Result<Photo>> Handle(Command request, CancellationToken cancellationToken)
            {
                var user = await _appUserRepository.GetUserWithPhoto();

                if (user == null) return null;

                var photoUploadResult = await _photoResolver.AddPhoto(request.File);

                var photo = new Photo { Url = photoUploadResult.Url, ID = photoUploadResult.PublicID };

                if (!user.Photos.Any(x => x.IsMain)) photo.IsMain = true;

                user.Photos.Add(photo);

                var result = await _appUserRepository.SaveAllAsync();

                if (result) return Result<Photo>.Success(photo);

                return Result<Photo>.Failure("Problem adding photo");
            }
        }
    }
}
