﻿using MediatR;
using Reactivities.Application.Core;
using Reactivities.Application.Interfaces;
using Reactivities.Domain.User;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Reactivities.Application.Photos
{
    public class DeletePhoto
    {
        public class Command : IRequest<Result<Unit>>
        {
            public string ID { get; set; }
        }

        public class Handler : IRequestHandler<Command, Result<Unit>>
        {
            private readonly IPhotoResolver _photoResolver;
            private readonly IAppUserRepository _appUserRepository;

            public Handler(IPhotoResolver photoResolver, IAppUserRepository appUserRepository)
            {
                _photoResolver = photoResolver;
                _appUserRepository = appUserRepository;
            }

            public async Task<Result<Unit>> Handle(Command request, CancellationToken cancellationToken)
            {
                var user = await _appUserRepository.GetUserWithPhoto();

                if (user == null) return null;

                var photo = user.Photos.FirstOrDefault(x => x.ID == request.ID);

                if (photo == null) return null;

                if (photo.IsMain) return Result<Unit>.Failure("You cannot delete your main photo");

                var result = await _photoResolver.DeletePhoto(photo.ID);

                if (result == null) return Result<Unit>.Failure("Problem deleting photo");

                user.Photos.Remove(photo);

                var success = await _appUserRepository.SaveAllAsync();

                if (success) return Result<Unit>.Success(Unit.Value);

                return Result<Unit>.Failure("Problem deleting photo from API");
            }
        }
    }
}
