﻿using AutoMapper;
using Reactivities.Domain;
using Reactivities.Models.Attendee;
using System.Linq;

namespace Reactivities.Data.Core.ActivityAttendance.Mapper
{
    public class ActivityAttendeeMapper : Profile
    {
        public ActivityAttendeeMapper()
        {
            string currentUsername = null;

            CreateMap<ActivityAttendee, AttendeeModel>()
                .ForMember(d => d.DisplayName, o => o.MapFrom(s => s.AppUser.DisplayName))
                .ForMember(d => d.Username, o => o.MapFrom(s => s.AppUser.UserName))
                .ForMember(d => d.Bio, o => o.MapFrom(s => s.AppUser.Bio))
                .ForMember(d => d.Image, o => o.MapFrom(s => s.AppUser.Photos.FirstOrDefault(x => x.IsMain).Url))
                .ForMember(d => d.FollowersCount, o => o.MapFrom(s => s.AppUser.Followers.Count))
                .ForMember(d => d.FollowingCount, o => o.MapFrom(s => s.AppUser.Followings.Count))
                .ForMember(d => d.Following, o => o.MapFrom(s => s.AppUser.Followers.Any(x => x.Observer.UserName == currentUsername)));
        }
    }
}
