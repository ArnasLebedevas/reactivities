﻿using MediatR;
using Reactivities.Application.Core;
using Reactivities.Domain;
using Reactivities.Domain.Activities;
using Reactivities.Domain.User;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Reactivities.Application.ActivityAttendance
{
    public class UpdateActivityAttendance
    {
        public class Command : IRequest<Result<Unit>>
        {
            public Guid ID { get; set; }
        }

        public class Handler : IRequestHandler<Command, Result<Unit>>
        {
            private readonly IActivityRepository _activityRepository;
            private readonly IAppUserRepository _appUserRepository;

            public Handler(IActivityRepository activityRepository, IAppUserRepository appUserRepository)
            {
                _activityRepository = activityRepository;
                _appUserRepository = appUserRepository;
            }

            public async Task<Result<Unit>> Handle(Command request, CancellationToken cancellationToken)
            {
                var activity = await _activityRepository.GetActivityAttendanceAsync(request.ID);

                if (activity == null) return null;

                var user = await _appUserRepository.GetUser();

                if (user == null) return null;

                var hostUsername = activity.Attendees.FirstOrDefault(x => x.IsHost)?.AppUser?.UserName;

                var attendance = activity.Attendees.FirstOrDefault(x => x.AppUser.UserName == user.UserName);

                if (attendance != null && hostUsername == user.UserName)
                    activity.IsCancelled = !activity.IsCancelled;

                if (attendance != null && hostUsername != user.UserName)
                    activity.Attendees.Remove(attendance);

                if(attendance == null)
                {
                    attendance = new ActivityAttendee { AppUser = user, Activity = activity, IsHost = false };

                    activity.Attendees.Add(attendance);
                }

                var result = await _activityRepository.SaveAllAsync();

                return result ? Result<Unit>.Success(Unit.Value) : Result<Unit>.Failure("Problem updating attendance");
            }
        }
    }
}
