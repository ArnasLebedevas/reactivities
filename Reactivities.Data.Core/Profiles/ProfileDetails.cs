﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Reactivities.Application.Core;
using Reactivities.Application.Interfaces;
using Reactivities.Models.Profile;
using Reactivities.Persistence;
using System.Threading;
using System.Threading.Tasks;

namespace Reactivities.Application.Profiles
{
    public class ProfileDetails
    {
        public class Query : IRequest<Result<ProfileModel>>
        {
            public string Username { get; set; }
        }

        public class Handler : IRequestHandler<Query, Result<ProfileModel>>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            private readonly IUserResolver _userResolver;

            public Handler(DataContext context, IMapper mapper, IUserResolver userResolver)
            {
                _context = context;
                _mapper = mapper;
                _userResolver = userResolver;
            }

            public async Task<Result<ProfileModel>> Handle(Query request, CancellationToken cancellationToken)
            {
                var user = await _context.Users
                     .ProjectTo<ProfileModel>(_mapper.ConfigurationProvider, new { currentUsername = _userResolver.GetUsername() })
                     .SingleOrDefaultAsync(x => x.Username == request.Username);

                return Result<ProfileModel>.Success(user);
            }
        }
    }
}
