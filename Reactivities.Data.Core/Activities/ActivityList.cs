﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Reactivities.Application.Activities.Params;
using Reactivities.Application.Core;
using Reactivities.Application.Core.Paging;
using Reactivities.Application.Interfaces;
using Reactivities.Models.Activity;
using Reactivities.Persistence;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Reactivities.Application.Activities
{
    public class ActivityList
    {
        public class Query : IRequest<Result<PagedList<ActivityModel>>>
        {
            public ActivityParams Params { get; set; }
        }

        public class Handler : IRequestHandler<Query, Result<PagedList<ActivityModel>>>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            private readonly IUserResolver _userResolver;

            public Handler(DataContext context, IMapper mapper, IUserResolver userResolver)
            {
                _context = context;
                _mapper = mapper;
                _userResolver = userResolver;
            }

            public async Task<Result<PagedList<ActivityModel>>> Handle(Query request, CancellationToken cancellationToken)
            {
                var query = _context.Activities
                    .Where(d => d.Date >= request.Params.StartDate)
                    .OrderBy(d => d.Date)
                    .ProjectTo<ActivityModel>(_mapper.ConfigurationProvider, new { currentUsername = _userResolver.GetUsername() })
                    .AsQueryable();

                if(request.Params.IsGoing && !request.Params.IsHost)
                    query = query.Where(x => x.Attendees.Any(a => a.Username == _userResolver.GetUsername()));

                if (request.Params.IsHost && !request.Params.IsGoing)
                    query = query.Where(x => x.HostUsername == _userResolver.GetUsername());

                return Result<PagedList<ActivityModel>>.Success(
                    await PagedList<ActivityModel>.CreateAsync(query, request.Params.PageNumber, request.Params.PageSize));
            }
        }
    }
}
