﻿using Microsoft.EntityFrameworkCore;
using Reactivities.Domain;
using Reactivities.Domain.Activities;
using Reactivities.Persistence;
using System;
using System.Threading.Tasks;

namespace Reactivities.Data.Core.Activities
{
    public class ActivityRepository : Repository<Activity>, IActivityRepository
    {
        public ActivityRepository(DataContext context) : base (context)
        { }

        public async Task<Activity> GetAsync(Guid id)
        {
            return await _context.Activities.FindAsync(id);
        }

        public async Task<Activity> GetActivityAttendanceAsync(Guid id)
        {
            return await _context.Activities.Include(a => a.Attendees).ThenInclude(u => u.AppUser).FirstOrDefaultAsync(x => x.ID == id);
        }
    }
}
