﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Reactivities.Application.Core;
using Reactivities.Application.Interfaces;
using Reactivities.Models.Activity;
using Reactivities.Persistence;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Reactivities.Application.Activities
{
    public class DetailsActivity
    {
        public class Query : IRequest<Result<ActivityModel>>
        {
            public Guid ID { get; set; }
        }

        public class Handler : IRequestHandler<Query, Result<ActivityModel>>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            private readonly IUserResolver _userResolver;

            public Handler(DataContext context, IMapper mapper, IUserResolver userResolver)
            {
                _context = context;
                _mapper = mapper;
                _userResolver = userResolver;
            }

            public async Task<Result<ActivityModel>> Handle(Query request, CancellationToken cancellationToken)
            {
                var activity = await _context.Activities
                    .ProjectTo<ActivityModel>(_mapper.ConfigurationProvider, new { currentUsername = _userResolver.GetUsername() })
                    .FirstOrDefaultAsync(x => x.ID == request.ID);

                return Result<ActivityModel>.Success(activity);
            }
        }
    }
}
