﻿using AutoMapper;
using Reactivities.Domain;
using Reactivities.Models.Activity;
using System.Linq;

namespace Reactivities.Data.Core.Activities.Mapper
{
    public class ActivityMapper : Profile
    {
        public ActivityMapper()
        {
            CreateMap<Activity, Activity>();

            CreateMap<Activity, ActivityModel>()
                .ForMember(d => d.HostUsername, o => o.MapFrom(s => s.Attendees
                .FirstOrDefault(x => x.IsHost).AppUser.UserName));
        }
    }
}
