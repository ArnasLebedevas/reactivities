﻿using FluentValidation;
using MediatR;
using Reactivities.Application.Core;
using Reactivities.Domain;
using Reactivities.Domain.Activities;
using Reactivities.Domain.User;
using System.Threading;
using System.Threading.Tasks;

namespace Reactivities.Application.Activities
{
    public class CreateActivity
    {
        public class Command : IRequest<Result<Unit>>
        {
            public Activity Activity { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Activity).SetValidator(new ActivityValidator());
            }
        }

        public class Handler : IRequestHandler<Command, Result<Unit>>
        {
            private readonly IAppUserRepository _appUserRepository;
            private readonly IActivityRepository _activityRepository;

            public Handler(IAppUserRepository appUserRepository, IActivityRepository activityRepository)
            {
                _appUserRepository = appUserRepository;
                _activityRepository = activityRepository;
            }

            public async Task<Result<Unit>> Handle(Command request, CancellationToken cancellationToken)
            {
                var user = await _appUserRepository.GetUser();

                var attendee = new ActivityAttendee 
                {
                    AppUser = user,
                    Activity = request.Activity,
                    IsHost = true 
                };

                request.Activity.Attendees.Add(attendee);

                _activityRepository.Add(request.Activity);

                var result = await _activityRepository.SaveAllAsync();

                if (!result) return Result<Unit>.Failure("Failed to create activity");

                return Result<Unit>.Success(Unit.Value);
            }
        }
    }
}
