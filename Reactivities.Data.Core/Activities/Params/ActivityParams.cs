﻿using Reactivities.Application.Core.Paging;
using System;

namespace Reactivities.Application.Activities.Params
{
    public class ActivityParams : PagingParams
    {
        public bool IsGoing { get; set; }
        public bool IsHost { get; set; }
        public DateTime StartDate { get; set; } = DateTime.UtcNow;
    }
}
