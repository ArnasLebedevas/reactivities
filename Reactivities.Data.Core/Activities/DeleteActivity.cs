﻿using MediatR;
using Reactivities.Application.Core;
using Reactivities.Domain.Activities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Reactivities.Application.Activities
{
    public class DeleteActivity
    {
        public class Command : IRequest<Result<Unit>>
        {
            public Guid ID { get; set; }
        }

        public class Handler : IRequestHandler<Command, Result<Unit>>
        {
            private readonly IActivityRepository _repository;

            public Handler(IActivityRepository repository)
            {
                _repository = repository;
            }

            public async Task<Result<Unit>> Handle(Command request, CancellationToken cancellationToken)
            {
                var activity = await _repository.GetAsync(request.ID);

                _repository.Remove(activity);

                var result = await _repository.SaveAllAsync();

                if (!result) return Result<Unit>.Failure("Failed to delete the activity");

                return Result<Unit>.Success(Unit.Value);
            }
        }
    }
}
