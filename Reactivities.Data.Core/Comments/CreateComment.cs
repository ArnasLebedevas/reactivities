﻿using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Reactivities.Application.Core;
using Reactivities.Application.Interfaces;
using Reactivities.Domain;
using Reactivities.Models.Comment;
using Reactivities.Persistence;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Reactivities.Application.Comments
{
    public class CreateComment
    {
        public class Command : IRequest<Result<CommentModel>>
        {
            public string Body { get; set; }
            public Guid ActivityID { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Body).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<Command, Result<CommentModel>>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            private readonly IUserResolver _userResolver;

            public Handler(DataContext context, IMapper mapper, IUserResolver userResolver)
            {
                _context = context;
                _mapper = mapper;
                _userResolver = userResolver;
            }

            public async Task<Result<CommentModel>> Handle(Command request, CancellationToken cancellationToken)
            {
                var activity = await _context.Activities.FindAsync(request.ActivityID);

                if (activity == null) return null;

                var user = await _context.Users
                    .Include(p => p.Photos)
                    .SingleOrDefaultAsync(x => x.UserName == _userResolver.GetUsername());

                var comment = new Comment { Author = user, Activity = activity, Body = request.Body };

                activity.Comments.Add(comment);

                var success = await _context.SaveChangesAsync() > 0;

                if (success) return Result<CommentModel>.Success(_mapper.Map<CommentModel>(comment));

                return Result<CommentModel>.Failure("Failed to add comment");
            }
        }
    }
}
