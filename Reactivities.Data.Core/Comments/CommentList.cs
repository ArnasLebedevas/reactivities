﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Reactivities.Application.Core;
using Reactivities.Models.Comment;
using Reactivities.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Reactivities.Application.Comments
{
    public class CommentList
    {
        public class Query : IRequest<Result<List<CommentModel>>>
        {
            public Guid ActivityID { get; set; }
        }

        public class Handler : IRequestHandler<Query, Result<List<CommentModel>>>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;

            public Handler(DataContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<Result<List<CommentModel>>> Handle(Query request, CancellationToken cancellationToken)
            {
                var comments = await _context.Comments
                    .Where(x => x.Activity.ID == request.ActivityID)
                    .OrderByDescending(x => x.CreatedAt)
                    .ProjectTo<CommentModel>(_mapper.ConfigurationProvider)
                    .ToListAsync();

                return Result<List<CommentModel>>.Success(comments);
            }
        }
    }
}
