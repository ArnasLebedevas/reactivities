﻿using System;

namespace Reactivities.Models.Comment
{
    public class CommentModel
    {
        public int ID { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Body { get; set; }
        public string Username { get; set; }
        public string DisplayName { get; set; }
        public string Image { get; set; }
    }
}
