﻿using Reactivities.Domain;
using System.Collections.Generic;

namespace Reactivities.Models.Profile
{
    public class ProfileModel
    {
        public string Username { get; set; }
        public string DisplayName { get; set; }
        public string Bio { get; set; }
        public string Image { get; set; }
        public bool Following { get; set; }
        public int FollowersCount { get; set; }
        public int FollowingCount { get; set; }

        public ICollection<Photo> Photos { get; set; }
    }
}
