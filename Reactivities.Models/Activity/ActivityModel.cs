﻿using Reactivities.Models.Attendee;
using System;
using System.Collections.Generic;

namespace Reactivities.Models.Activity
{
    public class ActivityModel
    {
        public Guid ID { get; set; }
        public string Title { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public string City { get; set; }
        public string Venue { get; set; }
        public string HostUsername { get; set; }
        public bool IsCancelled { get; set; }

        public ICollection<AttendeeModel> Attendees { get; set; }
    }
}
