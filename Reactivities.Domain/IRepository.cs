﻿using System.Threading.Tasks;

namespace Reactivities.Domain
{
    public interface IRepository<TEntity>
    {
        void Add(TEntity entity);
        void Remove(TEntity entity);
        Task<bool> SaveAllAsync();
    }
}
