﻿using System.Threading.Tasks;

namespace Reactivities.Domain.UserFollowings
{
    public interface IUserFollowingRepository : IRepository<UserFollowing>
    {
        Task<UserFollowing> FindAsync(string observerID, string targetID);
    }
}
