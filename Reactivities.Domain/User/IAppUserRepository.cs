﻿using System.Threading.Tasks;

namespace Reactivities.Domain.User
{
    public interface IAppUserRepository : IRepository<AppUser>
    {
        Task<AppUser> GetUser();
        Task<AppUser> GetUserWithPhoto();
        Task<AppUser> GetUserByUsername(string username);
    }
}
