﻿using System;
using System.Threading.Tasks;

namespace Reactivities.Domain.Activities
{
    public interface IActivityRepository : IRepository<Activity>
    {
        Task<Activity> GetAsync(Guid id);
        Task<Activity> GetActivityAttendanceAsync(Guid id);
    }
}
