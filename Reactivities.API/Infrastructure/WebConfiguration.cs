﻿using Microsoft.AspNetCore.Authorization;
using Reactivities.API.Infrastructure.Photos;
using Reactivities.API.Infrastructure.Security;
using Reactivities.API.Infrastructure.Services;
using Reactivities.Application.Interfaces;
using Reactivities.Data.Core.Activities;
using Reactivities.Data.Core.Followers;
using Reactivities.Data.Core.User;
using Reactivities.Domain.Activities;
using Reactivities.Domain.User;
using Reactivities.Domain.UserFollowings;

namespace Reactivities.API.Infrastructure
{
    public class WebConfiguration : IServiceConfiguration
    {
        public void Configure(IServices services)
        {
            services.AddTransient<IActivityRepository, ActivityRepository>();
            services.AddTransient<IPhotoResolver, PhotoResolver>();
            services.AddTransient<IAppUserRepository, AppUserRepository>();
            services.AddTransient<IUserResolver, UserResolver>();
            services.AddTransient<IAuthorizationHandler, IsHostRequirementHandler>();
            services.AddTransient<IUserFollowingRepository, UserFollowingRepository>();
        }
    }
}
