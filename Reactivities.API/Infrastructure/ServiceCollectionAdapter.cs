﻿using Microsoft.Extensions.DependencyInjection;
using Reactivities.API.Infrastructure.Services;

namespace Reactivities.API.Infrastructure
{
    public class ServiceCollectionAdapter : IServices
    {
        private readonly IServiceCollection _serviceCollection;

        public ServiceCollectionAdapter(IServiceCollection serviceCollection)
        {
            _serviceCollection = serviceCollection;
        }

        public void AddTransient<TService, TImplementation>()
            where TService : class
            where TImplementation : class, TService
        {
            _serviceCollection.AddTransient<TService, TImplementation>();
        }

        public void AddTransient<TImplementation>()
            where TImplementation : class
        {
            _serviceCollection.AddTransient<TImplementation>();
        }

        public void AddScoped<TService, TImplementation>()
            where TService : class
            where TImplementation : class, TService
        {
            _serviceCollection.AddScoped<TService, TImplementation>();
        }
    }
}
