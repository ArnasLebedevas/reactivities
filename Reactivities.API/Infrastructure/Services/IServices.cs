﻿namespace Reactivities.API.Infrastructure.Services
{
    public interface IServices
    {
        void AddTransient<TService, TImplementation>()
            where TService : class
            where TImplementation : class, TService;

        void AddTransient<TImplementation>()
            where TImplementation : class;

        void AddScoped<TService, TImplementation>()
            where TService : class
            where TImplementation : class, TService;
    }
}
