﻿using Microsoft.AspNetCore.Http;
using Reactivities.Application.Interfaces;
using System.Security.Claims;

namespace Reactivities.API.Infrastructure.Security
{
    public class UserResolver : IUserResolver
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserResolver(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string GetUsername()
        {
            return _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.Name);
        }
    }
}
