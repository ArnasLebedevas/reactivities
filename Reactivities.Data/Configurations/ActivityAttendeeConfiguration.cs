﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reactivities.Domain;

namespace Reactivities.Persistence.Configurations
{
    public class ActivityAttendeeConfiguration : IEntityTypeConfiguration<ActivityAttendee>
    {
        public void Configure(EntityTypeBuilder<ActivityAttendee> builder)
        {
            builder.HasKey(aa => new { aa.AppUserID, aa.ActivityID });

            builder
                .HasOne(u => u.AppUser)
                .WithMany(a => a.Activities)
                .HasForeignKey(aa => aa.AppUserID);

            builder
               .HasOne(u => u.Activity)
               .WithMany(a => a.Attendees)
               .HasForeignKey(aa => aa.ActivityID);
        }
    }
}
